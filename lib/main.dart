import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.grey[900]);
    return ChangeNotifierProvider<QuizBoardProvider>(
      builder: (context) => QuizBoardProvider(),
      child: MaterialApp(
        title: 'Quizzler',
        home: Scaffold(
          body: SafeArea(
            child: QuizBoard(),
          ),
        ),
      ),
    );
  }
}

class QuizBoard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final quizBoardProvider = Provider.of<QuizBoardProvider>(context);
    return Container(
      color: Colors.grey[900],
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
              flex: 12,
              child: QuizQuestion(quizBoardProvider
                  .questions[quizBoardProvider.getQuestionIndex()].text)),
          Expanded(
            flex: 1,
            child: QuizScoreKeeper(),
          ),
          Expanded(
            flex: 6,
            child: QuizButtons(),
          ),
        ],
      ),
    );
  }
}

class QuizButtons extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: QuizButton('True', Colors.green, true),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: QuizButton('False', Colors.red, false),
          ),
        ),
      ],
    );
  }
}

class QuizButton extends StatelessWidget {
  final String text;
  final bool answerValue;
  final Color color;

  QuizButton(this.text, this.color, this.answerValue);

  @override
  Widget build(BuildContext context) {
    final quizBoardProvider = Provider.of<QuizBoardProvider>(context);
    return RaisedButton(
      color: color,
      onPressed: () => {
            quizBoardProvider.answerQuestionWith(answerValue),
            if (quizBoardProvider.hasFinished())
              {
                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text(
                            'You\'ve finished the quiz! (Correct answers: ${quizBoardProvider.numberOfCorrectAnswers()} / ${quizBoardProvider.questions.length})'),
                        actions: <Widget>[
                          FlatButton(
                            onPressed: () {
                              quizBoardProvider.resetBoard();
                              Navigator.of(context).pop();
                            },
                            child: Text('OK'),
                          )
                        ],
                      );
                    }),
              },
          },
      child: Text(
        text,
        style: TextStyle(color: Colors.white),
      ),
    );
  }
}

class QuizQuestion extends StatelessWidget {
  final String question;

  QuizQuestion(this.question);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text(
          question,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 24, color: Colors.white70),
        ),
      ),
    );
  }
}

class QuizScoreKeeper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final QuizBoardProvider quizBoardProvider =
        Provider.of<QuizBoardProvider>(context);
    List<Widget> scoreIndicators = [];
    for (int i = 0; i < quizBoardProvider.answers().length; i++) {
      if (quizBoardProvider.answers()[i]) {
        scoreIndicators.add(CorrectQuizScoreIndicator());
      } else {
        scoreIndicators.add(WrongQuizScoreIndicator());
      }
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[...scoreIndicators],
      ),
    );
  }
}

class CorrectQuizScoreIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Icon(
      Icons.check,
      color: Colors.green,
    );
  }
}

class WrongQuizScoreIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Icon(
      Icons.close,
      color: Colors.red,
    );
  }
}

class Question {
  final String text;
  final bool correctAnswer;

  Question(this.text, this.correctAnswer);
}

class QuizBoardProvider extends ChangeNotifier {
  final List<Question> questions = [
    Question('Some cats are actually allergic to humans', true),
    Question('You can lead a cow down stairs but not up stairs.', false),
    Question('Approximately one quarter of human bones are in the feet.', true),
    Question('A slug\'s blood is green.', true),
    Question('Buzz Aldrin\'s mother\'s maiden name was \"Moon\".', true),
    Question('It is illegal to pee in the Ocean in Portugal.', true),
    Question(
        'No piece of square dry paper can be folded in half more than 7 times.',
        false),
    Question(
        'In London, UK, if you happen to die in the House of Parliament, you are technically entitled to a state funeral, because the building is considered too sacred a place.',
        true),
    Question(
        'The loudest sound produced by any animal is 188 decibels. That animal is the African Elephant.',
        false),
    Question(
        'The total surface area of two human lungs is approximately 70 square metres.',
        true),
    Question('Google was originally called \"Backrub\".', true),
    Question(
        'Chocolate affects a dog\'s heart and nervous system; a few ounces are enough to kill a small dog.',
        true),
    Question(
        'In West Virginia, USA, if you accidentally hit an animal with your car, you are free to take it home to eat.',
        true),
  ];

  List<bool> _questionAnswers = [];
  int _currentQuestionIndex = 0;
  bool _hasFinished = false;

  get currentQuestionIndex => _currentQuestionIndex;
  get questionAnswers => _questionAnswers;

  void answerQuestionWith(bool response) {
    if (response == questions[_currentQuestionIndex].correctAnswer) {
      _questionAnswers.add(true);
    } else {
      _questionAnswers.add(false);
    }
    int nextIndex = _currentQuestionIndex + 1;
    if (nextIndex > questions.length - 1) {
      nextIndex = _currentQuestionIndex;
      _hasFinished = true;
    }
    _currentQuestionIndex = nextIndex;
    notifyListeners();
  }

  void resetBoard() {
    _hasFinished = false;
    _questionAnswers = [];
    _currentQuestionIndex = 0;
    notifyListeners();
  }

  int getQuestionIndex() {
    return _currentQuestionIndex;
  }

  bool hasFinished() {
    return _hasFinished;
  }

  int numberOfCorrectAnswers() {
    int correctAnswers = 0;
    for (int index = 0; index < _questionAnswers.length; index++) {
      if (_questionAnswers[index] == true) {
        correctAnswers++;
      }
    }
    return correctAnswers;
  }

  List<bool> answers() {
    return _questionAnswers;
  }
}
